# Projet de TP : Serveur d'authentification en Go

Ce projet implémente un serveur d'authentification en Go, ainsi qu'un client utilisateur et un serveur de ressources pour démontrer un flux d'authentification simple. Le serveur d'authentification génère des codes d'authentification aléatoires, les stocke temporairement et les vérifie lorsqu'ils sont soumis par le client utilisateur.

## Structure du projet

Le projet est structuré en trois composants principaux :

1. **UserClient** : Ce composant est un client utilisateur en ligne de commande. Il demande un code d'authentification à l'utilisateur, l'envoie au serveur d'authentification et affiche le résultat.
  - `main.go` : Le fichier principal du client utilisateur qui interagit avec le serveur d'authentification.
  - `Dockerfile` : Fichier pour la construction de l'image Docker du client utilisateur.
  - `go.mod` : Fichier de module Go pour les dépendances du client utilisateur.

2. **AuthServer** : Ce composant génère et stocke les codes d'authentification, ainsi que leur expiration. Il expose une API pour générer et vérifier les codes.
  - `main.go` : Le fichier principal du serveur d'authentification qui définit les endpoints et leur logique.
  - `token.go` : Le fichier contenant les fonctions pour générer et vérifier les tokens OTP.
  - `Dockerfile` : Fichier pour la construction de l'image Docker du serveur d'authentification.
  - `go.mod` : Fichier de module Go pour les dépendances du serveur d'authentification.

3. **ResourceServer** : Ce composant est un serveur de ressources qui vérifie les codes d'authentification soumis par les clients utilisateurs. Il communique avec le serveur d'authentification pour valider les codes.
  - `main.go` : Le fichier principal du serveur de ressources qui définit les endpoints et leur logique.
  - `token.go` : Le fichier contenant les fonctions pour vérifier les tokens OTP.
  - `Dockerfile` : Fichier pour la construction de l'image Docker du serveur de ressources.
  - `go.mod` : Fichier de module Go pour les dépendances du serveur de ressources.

`docker-compose.yml` : Fichier de configuration Docker Compose pour exécuter l'ensemble du projet.

## Installation et exécution

### Instructions
1. Clonez ce dépôt sur votre machine locale.
2. Assurez-vous que Docker et Docker Compose sont installés sur votre machine.
3. Naviguez vers le répertoire du projet cloné.
4. Exécutez la commande suivante pour construire et démarrer les conteneurs Docker :
    ```bash
    docker-compose up
    ```
5. Les composants seront construits et déployés en utilisant Docker. Vous pouvez accéder aux différents services via les ports spécifiés dans le fichier `docker-compose.yml`.